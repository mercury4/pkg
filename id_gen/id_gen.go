package id_gen

import (
	"fmt"
//	"time"

	"github.com/sony/sonyflake"
)

var (
	sonyFlake     *sonyflake.Sonyflake
	sonyMachineID uint16
)

func init() {

	settings := sonyflake.Settings{}
	//settings.StartTime = time.Now()
	sonyFlake = sonyflake.NewSonyflake(settings)

	return

}

func GetId() (id uint64, err error) {
	if sonyFlake == nil {
		err = fmt.Errorf("snoy flake not inited")
		return

	}

	id, err = sonyFlake.NextID()
	return
}
