package id_gen

import (
	"testing"
)

func TestGetId(t *testing.T) {

	for i := 0; i < 10; i++ {
		id, err := GetId()
		if err != nil {
			t.Errorf("get id error:%v", err)
			return
		}

		t.Logf("get id succ, id:%v", id)
	}
}
