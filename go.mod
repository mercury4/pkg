module gitlab.com/mercury4/pkg

go 1.13

require (
	github.com/go-redis/redis/v8 v8.0.0-beta.3
	github.com/sony/sonyflake v1.0.0
)
