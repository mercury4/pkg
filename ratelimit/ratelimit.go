package ratelimit

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

type RateLimit struct {
	limit    int
	duration time.Duration
	key      string
	client   *redis.Client
}

func NewRateLimit(client *redis.Client, key string, limit int, duration time.Duration) *RateLimit {

	return &RateLimit{
		limit:    limit,
		duration: duration,
		client:   client,
		key:      key,
	}
}

func (r *RateLimit) Allow(ctx context.Context) bool {

	current, err := r.client.Incr(ctx, r.key).Result()
	if err != nil {
		return true
	}

	//第一个设置的
	if current == 1 {
		r.client.Expire(ctx, r.key, r.duration)
		return true
	}

	if current > int64(r.limit) {
		ttl, err := r.client.TTL(ctx, r.key).Result()
		if err != nil {
			return false
		}

		//不过期（说明前面设置失败了）
		//再设置一次
		if ttl == -1 {
			r.client.Expire(ctx, r.key, r.duration)
		}
		return false
	}
	return true
}
